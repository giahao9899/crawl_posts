<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/danh-muc/{id}', 'HomeController@index')->name('home.category');
Route::get('/bai-viet/{slug}', 'HomeController@show')->name('home.post');
