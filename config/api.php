<?php

return [
   'token' => env('TOKEN_USER', ''),

    'login' => 'https://api.9houz.com/api/user/login',

    'create' => [
        'blog' => 'https://api.9houz.com/api/bai-viet/create',
        'discuss' => 'https://api.9houz.com/api/discuss',
        'answer' => 'https://api.9houz.com/api/discuss/answer'
    ],
];
