<?php

namespace App\Console\Commands;

use App\Models\Category;
use Illuminate\Console\Command;

class ClassifyTuoiXayNha extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:ClassifyTuoiXayNha';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $categories = Category::where('parent_id', 26)->chunk(10, function ($categories) {
            foreach ($categories as $category) {
                $category->posts()->chunk(10, function ($posts) {
                    foreach ($posts as $post) {
                        $re = "/([0-9]+) xây nhà năm ([0-9]+)/";
                        preg_match($re, $post->title, $matches);
//                        dd($matches);
                        if ($matches) {
                            dump($matches[1] . " - " . $matches[2]);
                            $post->namsinh = $matches[1];
                            $post->namxay = $matches[2];
                            $post->save();
                        } else {
                            $this->error($post->slug);
                        }
                    }
                });
            }
        });
    }
}
