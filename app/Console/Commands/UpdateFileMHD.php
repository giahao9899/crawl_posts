<?php

namespace App\Console\Commands;

use App\Models\Category;
use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;

class UpdateFileMHD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:MHD';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Category::findOrFail(22)->posts()->chunkById(10,
            function ($posts) {
                foreach ($posts as $post) {
                    $crawler = new Crawler($post->content);
                    $this->info($post->slug);
                    $crawler->filter('a')->each(function (Crawler $node) use ($post) {
                        $link = $node->attr('href');
                        if ($link === null | $link === '#') {
                            return;
                        }

                        $this->line($link);

                        $post->file = $link;
                        $post->save();

                    });
                }
            }
        );
    }
}
