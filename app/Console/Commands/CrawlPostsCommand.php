<?php

namespace App\Console\Commands;

use App\Crawler\Post\PostCrawler;
use Illuminate\Console\Command;

class CrawlPostsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:posts
     {--web=: Website crawl}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command Crawl Post';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $web = $this->option('web');
        if(!empty($web)) {
            $crawler = new PostCrawler();
            $crawler->crawled();
        } else {
            $this->error("I done understand you :?");
        }
    }
}
