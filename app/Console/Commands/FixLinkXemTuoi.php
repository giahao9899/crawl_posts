<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Symfony\Component\DomCrawler\Crawler;

class FixLinkXemTuoi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:fixLinkXemTuoi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $categories = Category::findOrFail(26);
        foreach ($categories->children as $category) {
            $category->posts()->chunkById(10, function ($posts) {
                foreach ($posts as $post) {
//                    $this->info($post->id);
                    $crawler = new Crawler($post->content);
                    $crawler->filter('a')->each(function (Crawler $node) {
                        $link = $node->attr('href');
                        $re = '/wpxd-tuoi-xay-dung\/([0-9]{4})-([0-9]{4})/';
                        preg_match($re, $link, $matches);
                        if ($matches) {
                            $this->info($matches[1] . '-' . $matches[2]);
                            $lunar_year = Str::slug(getLunarYear($matches[1]));
                            $slug = "/bai-viet/tuoi-$lunar_year-$matches[1]-xay-nha-nam-$matches[2]";

                            $node->getNode(0)->setAttribute('href', $slug);
                            $this->line("New:" . $node->attr('href'));
                        } else {
                            $this->error($link);
                            $re = '/[0-9]{4}/';
                            preg_match($re, $link, $matches);
                            $lunar_year = Str::slug(getLunarYear($matches[0]));
                            $slug = "/bai-viet/tuoi-nao-hop-lam-nha-nam-$lunar_year-$matches[0]";

                            $node->getNode(0)->setAttribute('href', $slug);
                            $this->line("New:" . $node->attr('href'));

                        }
                    });
                    $post->content = $crawler->html();
                    $post->save();
//                    $myfile = fopen("/home/vnper/Developer/test2.html", "w") or die("Unable to open file!");
//                fwrite($myfile, $crawler->html());
//                fclose($myfile);
//                dd('OK');
                }
            });
        }
    }
}
