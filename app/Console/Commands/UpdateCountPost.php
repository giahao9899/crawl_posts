<?php

namespace App\Console\Commands;

use App\Models\Category;
use Illuminate\Console\Command;

class UpdateCountPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'category:cacheCount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command update cache count post in laravel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $categories = Category::all();

        foreach ($categories as $category) {
            $countPost = $category->posts()->count();
            $this->info($countPost);
            $category->post_count = $countPost;
            if ($category->parent_id == null) {
                $category->type = 1;
            } else {
                $category->type = 2;
            }
            $category->save();
        }
    }
}
