<?php

namespace App\Console\Commands;

use App\Core\AutoLinker\AutoLinker;
use App\Models\Post;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class PushPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push:post
                            {--local_cate_id= : local cate id}
                            {--server_cate_id= : serve cate id}
                            {--file= : upload file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to push post';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $local_cate_id = $this->option('local_cate_id');
        $server_cate_id = $this->option('server_cate_id');
        $file = $this->option('file');
        $token = config('api.token');

        $client = new Client();

        Post::where('category_id', $local_cate_id)->chunkById(50, function ($posts) use ($token, $file, $server_cate_id, $client) {
            foreach ($posts as $post) {
                $this->info($post->title);
                try {
                    $AutoLinker = new AutoLinker(1);

                    $data = [
                        [
                            'name' => 'title',
                            'contents' => $post->title,
                        ],
                        [
                            'name' => 'content',
                            'contents' => $AutoLinker->processText($post->content),
                        ],
                        [
                            'name' => 'descriptions',
                            'contents' => $post->descriptions ?? $post->title,
                        ],
                        [
                            'name' => 'category_id',
                            'contents' => $server_cate_id,
                        ],
                    ];
                    if (isset($file)) {
                        $data[] = [
                                'name' => 'file',
                                'contents' => fopen("/home/giahao9899/Coder/crawl_posts/storage" . $post->file, 'r')
                            ];
                        $data[] = [
                                'name' => 'type_doc',
                                'contents' => $file
                            ];
                    }
                    $response = $client->request('POST',
                        config('api.create.blog'),
                        [
                            'headers' => [
                                'Authorization' => 'Bearer '.$token
                            ],
                            'multipart' => $data
                        ]
                    );
                    $this->info($response->getBody());
                    $this->info('Next --------------->');
                } catch (\Exception $e) {
                    $this->error('Fail upload: $post->id');
                    \Log::error("Fail upload: $post->id");
                }
            }
        });
    }
}
