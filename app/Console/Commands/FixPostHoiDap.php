<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Console\Command;

class FixPostHoiDap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:fixPostHoiDap {--option=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command fix post hỏi đáp';

    const DONE = 1;
    const FALSE = -1;
    const NOTFIX = 0;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $category = Category::whereName('Hỏi Đáp')->first();
        $category_id = $category->id;
//        dd($category_id);
        $option = $this->option('option');

        if ($option === 'remove_link') {
            Post::where('category_id', $category_id)->chunkById(100, function ($posts) {
               foreach ($posts as $post) {
                   $new_content = str_replace("http://boxaydung.vn",
                       "#",
                       $post->content);
                   $this->info($post->id);
                   $post->content = $new_content;
                   $post->save();
               }
            });
        } else {
            Post::where('category_id', $category_id)->chunk(100, function ($posts) {
                foreach ($posts as $post) {
                    $content = $post->content;
                    $re = '/(<p(( style="text-align: justify;")?)>)((<(?!\/)\w*>)+)(Về vấn đề(.*)như sau|Sau khi(.*)như sau).*$/';

                    preg_match($re, $content, $matches);

                    if (empty($matches)) {
                        dump($post->id);
                        $post->fix_status = self::FALSE;
                        $post->save();
                    } else {
                        $this->info($matches[0]);

                        $post->fix_status = self::DONE;
                        $post->save();
                    }
                }
                $this->info('NEXT.....');
            });
        }

    }
}
