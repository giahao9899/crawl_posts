<?php

namespace App\Console\Commands;

use App\Models\Category;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;

class DownloadMauHopDong extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:downloadHopDong';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Category::whereId(22)->first()->posts()->chunkById(10, function ($posts) {
           foreach ($posts as $post) {
               $crawler = new Crawler($post->content);
//               dump($post->slug);
               $crawler->filter('a')->each(function (Crawler $node) {
                   $link = $node->attr('href');
                   if($link === null) {
                       return;
                   }
                   $re = "/https:\/\/shac.vn\/wp-content\/uploads\/2019\/04\/Hop-dong/";
                   preg_match($re, $link, $matches);
                   if ($matches) {
                       $new_link = $this->downloadFile($link);
                       $node->getNode(0)->setAttribute('href', $new_link);
                       $this->line("New:" . $node->attr('href'));
                   } else {
                       $this->info($link);
                       $node->getNode(0)->setAttribute('href', "#");
                       $this->line("New:" . $node->attr('href'));
                        //set lai link
                   }
               });
               $post->content = $crawler->html();
               $post->save();
           }
        });
    }

    public function downloadFile($link){
        $client = new Client();
        try {
            $path_info = pathinfo($link);
//            dd($path_info['basename']);
            $file_path = fopen("storage/mau-hop-dong/" . $path_info['basename'],'w');
            $response = $client->get($link, ['save_to' => $file_path]);
            return "/mau-hop-dong/" .$path_info['basename'];
        } catch (\Exception $e) {
            $this->error("Error: $link");
        }

    }
}
