<?php

namespace App\Console\Commands;

use App\Core\AutoLinker\AutoLinker;
use App\Models\Category;
use App\Models\Post;
use App\Models\User9houz;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class PushHoiDap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push:HoiDap {--server_cate_id= : serve cate id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to push cate Hỏi Đáp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $server_cate_id = $this->option('server_cate_id');
        $category = Category::whereName('Hỏi Đáp')->first();
        $category_id = $category->id;

        $client = new Client();

        $server_id = 83;

        Post::where('category_id', $category_id)->where('fix_status', 1)->chunkById(50, function ($posts) use ($server_cate_id, $client, &$server_id) {
            foreach ($posts as $post) {
                $this->info($post->title);
                dump($server_id);
//                $server_discuss_id = $this->pushQuestion($client, $post, $server_cate_id);
//
//                if ($server_discuss_id) {
                    $this->pushAnswer($post, $server_id);
//                }
//                dd('OK');
                $server_id++;
            }
        });
    }

    public function getTokenUser() {
        $token = '';

        $user = User9houz::inRandomOrder()->first();
        $this->info($user->name);
        try {
            $client = new Client();
            $data = [
                'email' => $user->email,
                'password' => $user->password
            ];

            $response = $client->request('POST',
                config('api.login'),
                [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                    ],
                    'form_params' => $data
                ]
            );

            $res = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
            $token = $res['response']['token'];
        } catch (\Exception $exception) {
            throw new \Exception($exception);
        }

        return $token;
    }

    public function pushQuestion(Client $client, $post, $server_cate_id) {
        $token = config('api.token');
        try {
            $AutoLinker = new AutoLinker(1);

            $data = [
                'title' => $post->title,
                'content' => $AutoLinker->processText($post->content),
                'category' => $server_cate_id,
                'convert_type' => 'HTML'
            ];
            $response = $client->request('POST',
                config('api.create.discuss'),
                [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'Authorization' => 'Bearer '.$token
                    ],
                    'form_params' => $data
                ]
            );
            $res = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
            return $res['response']['data']['id'];
        } catch (\Exception $exception) {
            $this->error("Fail upload question: $post->id");
            \Log::error("Fail upload question: $post->id");
            return false;
        }
    }

    public function pushAnswer($post, $server_discuss_id) {
        $token = $this->getTokenUser();

        $client = new Client();

        try {
            $data = [
                'discuss_id' => $server_discuss_id,
                'content' => $post->answer,
                'convert_type' => 'HTML'
            ];

            $response = $client->request('POST',
                config('api.create.answer'),
                [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'Authorization' => 'Bearer '.$token
                    ],
                    'form_params' => $data
                ]
            );
            $this->info("Answer: " . $response->getStatusCode());
        } catch (\Exception $ex) {
            $this->error("Fail upload answer: $post->id");
            \Log::error("Fail upload answer: $post->id");
        }
    }

}
