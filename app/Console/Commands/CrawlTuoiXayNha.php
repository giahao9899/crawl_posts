<?php

namespace App\Console\Commands;

use App\Crawler\Post\PostBD;
use App\Crawler\Post\PostCrawler;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Symfony\Component\DomCrawler\Crawler;

class CrawlTuoiXayNha extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:TuoiXayNha';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command Crawl Tuổi xây nhà';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $client = new Client();
//        $response = $client->request('POST', 'https://shac.vn/wp-admin/admin-ajax.php', [
//            'form_params' => [
//                'namsinh' => '1999',
//                'namxay'  => '2020',
//                'action'  => 'wppt_ajax_tuoixaydung',
//                'wppt_nonce' => 'ff6ef04106'
//            ]
//        ]);
//        $post = $response->getBody()->getContents();
//        $post = $this->handlePost($post);
//        dd($post);

        $crawler = new PostCrawler();
//        dd(Str::slug($crawler->getLunarYear(1940)));
        for ($i = 2019; $i <= 2049; $i++) {
            for ($j = 2019; $j <= 2019; $j++) {
                $content = $crawler->CrawlTuoiXayNha("https://shac.vn/wpxd-tuoi-xay-dung/$j-$i");
                $content = $this->handlePost($content);
                $lunar_year = getLunarYear($j);
                $slug_lunar_year = Str::slug($lunar_year);
                $slug = "tuoi-$slug_lunar_year-$j-xay-nha-nam-$i";
                $title = "Xem tuổi $lunar_year $j xây nhà năm $i có tốt không giúp mang Tài
		Lộc đến";
                $this->line($title);

                PostBD::storePost($title, $content, $i, 1, $slug);
//                $myfile = fopen("/home/vnper/Developer/test2.html", "w") or die("Unable to open file!");
//                fwrite($myfile, $slug . $title . $content);
//                fclose($myfile);
//                dd('OK');
                $this->info('Next ....');
            }
        }
    }

    public function handlePost($post) {
//        dd($post);
        $crawler = new Crawler($post);
        for ($i = 1; $i < 5; $i++) {
            $crawler->filter('.page-content > *:nth-child(1)')->each(function (Crawler $crawler) {
                foreach ($crawler as  $node) {
                    $node->parentNode->removeChild($node);
                }
            });
        }
//        dd($crawler);
        return $this->fixContent($crawler->html());
    }

    public function fixContent($content) {
        $content = str_replace('SHAC', "<strong style='color:#b953a4'>9houz</strong>", $content);
        $content = str_replace("Cập nhật: 2016", "Cập nhật: 2019", $content);

        return $content;
    }
}
