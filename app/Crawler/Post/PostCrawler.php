<?php

namespace App\Crawler\Post;


use App\Models\Link;
use Openbuildings\Spiderling\Page;

class PostCrawler
{
    public function __construct() {
//        $this->url = "https://shac.vn/kien-thuc-nha-dep";
        $this->url = "https://shac.vn/tien-ich/hoi-dap";
    }

    public function crawled() {
        for ($i = 81; $i <= 100; $i++) {
            $page = new Page();
            $url = $i == 1 ? $this->url : "$this->url/page/$i";
            dump($url);
            $page->visit($url);
            $posts = $page->all('.post-item > a');
            foreach ($posts as $post) {
                $link = $post->attribute('href');
                dump("$link - (page: $i)" );
//                dd('OK');
                $this->visitPost($link);
            }
        }
    }

//    public function visit
    public function visitPost($link) {

        if (Link::where('url', $link)->exists()) {
            dump('=>>>>> Đã crawl');
            return;
        }
        $page = new Page();

        $page->visit($link);

        $category = $this->getCategory($page);
//        $category = 'Hỏi Đáp';

        $title = $this->getTitle($page);

        $content = $this->getContent($page);

        $post_id = PostBD::storePost($title, $content, $category);

        PostBD::storeLink($link, $post_id);

        dump($title);

    }

    public function checkNode($node) {
        try {
            $img = $node->find('img');
            return false;
        } catch (\Exception $exception) {
            if (in_array($node->tag_name(), ['p', 'ul', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'])) {
                if (strpos($node->text(), 'SHAC') === false && strpos($node->text(), 'Sơn Hà') === false
                && strpos($node->text(), 'TRA CỨU PHONG THỦY') === false && strpos($node->text(), 'PHẦN MỀM DỰ TOÁN CÔNG TRÌNH') === false) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public function getContent(Page $page) {
        $dom_content = $page->all(".post-content .entry-content > *:not(ul:last-child)");

        $content = '';

        foreach ($dom_content as $node) {
            if ( $this->checkNode($node)) {
//                $tag_name = $node->tag_name();
//                if (in_array($tag_name, ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'])) {
//                    $text_node = $node->text();
//                    $content .= "<$tag_name>$text_node</$tag_name>";
//                } else {
//                    $content .= $node->html();
//                }
                try {
                    $text = $node->text();
                    $img = $node->find('img');
                    $url_image = $img->attribute('data-lazy-src');
                    if (empty($url_image)) continue;
                    $width = $img->attribute('width');
                    $height = $img->attribute('width');
                    $alt = $img->attribute('alt');
                    $content .= "<p><img src='$url_image' alt='$alt' width='$width' height='$height'><br>$text</p>";
                } catch (\Exception $exception) {
                    $content .= $node->html();
                }
            }
        }

        $content = $this->editContent($content);

        return $content;
    }

    public function getTitle(Page $page) {
        try {
            $title = $page->find('.page-title')->text();
        } catch (\Exception $exception) {
            $title = '';
        }

        return $title;
    }

    public function getCategory(Page $page) {
        try {
            $category = $page->find('.breadcrumb li:nth-child(2) > span:nth-child(1)')->text();
        } catch (\Exception $exception) {
            $category = 'khác';
        }

        return $category;
    }

    public function editContent(string $content) {
//        $n = strripos($content, "<ul>");
//        $new_content = substr($content, 0, $n);
//        return $new_content;
        return $content;
    }

    public function CrawlTuoiXayNha($link) {
        $page = new Page();

        try {
            $page->visit($link);
            $content = $page->find('.page-content')->html();
            return $content;
        } catch (\Exception $exception) {
            dump("url: $link Error");
            return false;
        }
    }
}

