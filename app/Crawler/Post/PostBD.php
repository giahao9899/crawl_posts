<?php

namespace App\Crawler\Post;

use App\Models\Category;
use App\Models\Post;
use App\Models\Link;

class PostBD
{
    /**
     * Store Post
     *
     * @param $title
     * @param $content
     * @param $category_name
     * @param int $user_created
     * @param bool $slug
     * @return mixed
     */
    public static function storePost($title, $content, $category_name, $user_created = 1, $slug = false) {
        $post = new Post();

        $post->title = $title;
        if ($slug) {
            $post->slug = $slug;
        }
        $post->content = $content;
        $post->category_id = self::storeCategory($category_name);
        $post->created_by = $user_created;

        $post->save();

        return $post->id;
    }

    public static function storeCategory($category_name) {
        $category = Category::firstOrCreate(
          ['name' => $category_name]
        );

        return $category->id;
    }

    public static function storeLink($url, $post_id){
        $link = new Link();

        $link->url = $url;
        $link->post_id = $post_id;

        $link->save();
    }
}
