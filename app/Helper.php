<?php

if (!function_exists('getLunarYear')) {

    /**
     * Return Lunar Year from Year input
     *
     * @param $year
     * @return string
     */
    function getLunarYear($year) {
        $arr_can = array("Quý","Giáp","Ất","Bính","Đinh","Mậu","Kỷ","Canh","Tân","Nhâm");
        $arr_chi = array("Hợi","Tý","Sửu","Dần","Mão","Thìn","Tỵ","Ngọ","Mùi","Thân","Dậu","Tuất");

        $can = ($year - 3) % 10;
        $chi = ($year - 3) % 12;

        $LunarYear = $arr_can[$can] . " " . $arr_chi[$chi];
        return $LunarYear;
    }
}


if (!function_exists('inSpecChar')) {
    /**
     * Chuyển string thành dạng đặc biệt
     *
     * @param $str
     * @return string
     */
    function inSpecChar($str)
    {
        $strarr = S2A($str);
        $str = implode("<!---->", $strarr);
        return $str;
    }
}

if (!function_exists('reSpecChar')) {
    /**
     * Đưa str về bình thường
     *
     * @param $str
     * @return string
     */
    function reSpecChar($str)
    {
        $strarr = explode("<!---->", $str);
        $str = implode("", $strarr);
        $str = stripslashes($str);
        return $str;
    }
}


if (!function_exists('S2A')) {
    /**
     * Chuyển string thành mảng các char.
     *
     * @param $str
     * @return array
     */
    function S2A($str)
    {
        $chararray = array();
        for ($i = 0; $i < strlen($str); $i++) {
            array_push($chararray, $str{$i});
        }
        return $chararray;
    }
}

if (!function_exists('string_word_count')) {
    /**
     * Đếm số từ trong chuỗi
     *
     * @param $str
     * @return int
     */
    function string_word_count($str)
    {
        $str = str_replace("  ", " ", $str);
        $arr_str = explode(" ", $str);
        return count($arr_str);
    }
}

