<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PostRequest;
use App\Models\Category;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Illuminate\Http\Request;

/**
 * Class PostCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PostCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Post');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/post');
        $this->crud->setEntityNameStrings('post', 'posts');

        $this->crud->addClause('with', ['category', 'link']);
    }

    protected function setupListOperation()
    {
        $this->setupShowOperation();

        $this->crud->addFilter([
            'name'  => 'category_id',
            'type'  => 'select2_ajax',
            'label' => 'Danh mục',
            'placeholder' => 'Pick a category'
        ],
            route('admin.post.filter.category.ajax'),
            function($value) {
                if($value){
                    $this->crud->addClause('where', 'category_id', $value);
                }
            }
        );

        $this->crud->addFilter([
            'type' => 'text',
            'name' => 'filter_regex',
            'label'=> 'Regex'
        ],
            false,
            function($value) { // if the filter is active
                 $this->crud->addClause('where', 'title', 'regexp', $value);
            }
        );
        $this->crud->addFilter([
            'type' => 'select2',
            'name' => 'filter_fix_status',
            'label'=> 'Fix Status'
        ],
            function () {
                return [
                    1 => 'done',
                    -1 => 'false'
                ];
            },
            function($value) { // if the filter is active
                $this->crud->addClause('where', 'fix_status', $value);
            }
        );

        $this->crud->addFilter([
            'name'  => 'filter_trash',
            'type'  => 'select2',
            'label' => 'Trash'
        ], function () {
            return [
                1 => 'Đã xóa',
            ];
        }, function ($value) {
            //
            if ($value == 1) {
                $this->crud->addClause('onlyTrashed');
            }
        });
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PostRequest::class);

        $this->crud->addFields([
            [   // Title
                'name' => 'title',
                'label' => "Title",
                'type' => 'text',
            ],
            [  // Select
                'label' => "Category",
                'type' => 'select',
                'name' => 'category_id', // the db column for the foreign key
                'entity' => 'category', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => "App\Models\Category",
            ],
            [
                'name' => 'descriptions',
                'label' => 'Descriptions',
                'type' => 'ckeditor'
            ],
            [
                'name' => 'content',
                'label' => 'Content',
                'type' => 'ckeditor',
                'options' => [
                    'height' => '500px',
                ]
            ],
            [
                'name' => 'answer',
                'label' => 'Answer',
                'type' => 'ckeditor',
                'options' => [
                    'height' => '500px',
                ]
            ],

        ]);
    }

    protected function setupShowOperation() {
        $this->crud->addColumns([
            [
                'name' => 'id',
                'label' => "ID",
                'type' => 'text',
            ],
            [
                'name' => 'title',
                'label' => 'Title',
                'type' => 'text',
            ],
            [
                // 1-n relationship
                'label' => "Category", // Table column heading
                'type' => "select",
                'name' => 'category_id', // the column that contains the ID of that connected entity;
                'entity' => 'category', // the method that defines the relationship in your Model
                'attribute' => "name", // foreign key attribute that is shown to user
                'model' => "App\Models\Category", // foreign key model
            ],
            [
                'name' => 'link.url',
                'label' => 'Link crawl',
                'type' => 'text',
                'limit' => '255'
            ],
            [
                'name' => 'descriptions', // The db column name
                'label' => "Descriptions", // Table column heading
                'type' => 'markdown',
            ],
            [
                'name' => 'content', // The db column name
                'label' => "Content", // Table column heading
                'type' => 'markdown',
            ],
            [
                'name' => 'answer', // The db column name
                'label' => "Answer", // Table column heading
                'type' => 'markdown',
            ],
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }



    public function filterByCategory(Request $request)
    {
        $key = $request->input('term');
        $categories = Category::where('name','like','%'.$key.'%')->get();
        return $categories->pluck('name', 'id');
    }

}
