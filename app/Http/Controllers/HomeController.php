<?php

namespace App\Http\Controllers;

use App\Core\AutoLinker\AutoLinker;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index($category_id = 0) {
        $categories = Category::whereType(1)->get();
        if ($category_id == 0) {
            $posts = Post::paginate(25);
        } else {
            $posts = Post::where('category_id', $category_id)->paginate(25);
        }

        return view('frontend.post.index', compact([
            'categories',
            'posts'
        ]));
    }

    public function show($slug) {
        $post = Post::whereSlug($slug)->firstOrFail();
        $linker = new AutoLinker(1);
        $post_content = $linker->processText($post->content);

        return view('frontend.post.show', compact([
            'post',
            'post_content'
        ]));
    }
}
