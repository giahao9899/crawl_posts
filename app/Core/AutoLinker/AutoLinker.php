<?php
namespace App\Core\AutoLinker;

class AutoLinker
{
    /**
     * The maximum number of different links that will be generated per article
     * Số lượng tối đa các liên kết khác nhau(hay từ khóa) sẽ được tạo?
     *
     * @var integer
     */
    private $max_keyword;

    /**
     * The maximum number of links created with the same keyword
     * Từ khóa sẽ được lặp lại bao nhiêu lần?
     *
     * @var integer
     */
    private $max_single;

    /**
     * The attributes that are model Keyword
     */
    private $keyword = 'App\Models\Keyword';

    /**
     * Query regex
     *
     * @var string
     */
    private $reg = '/(?!(?:[^<\[]+[>\]]|[^>\]]+<\/a>))($name)/iUms';

    public function __construct(int $max_single = -1, int $max_keyword = null)
    {
        $this->setMaxSingle($max_single);
        $this->setMaxKeyword($max_keyword);
    }

    /**
     * function main, auto insert link
     *
     * @param $text
     * @return string
     */
    public function processText($text) {

        $max_single = $this->max_single;
        $max_keyword = $this->max_keyword;
        $keyword_insert = 1;
        $text = $this->tagHeadingDecode(html_entity_decode($text));
        $this->keyword::orderBy('type', 'desc')->chunk(100, function ($keywords) use (&$text, $max_keyword, $max_single, &$keyword_insert) {
            foreach ($keywords as $keyword) {
//                dump(!$max_keyword || $keyword_insert <= $max_keyword);
                if (!$max_keyword || $keyword_insert <= $max_keyword) {
                    $replace = $this->createReplace($keyword->url, true);
                    $regexp = str_replace('$name', $keyword->name, $this->reg);
                    $new_text = preg_replace($regexp, $replace, $text, $max_single);
                    if ($new_text != $text) {
                        $keyword_insert++;
                        $text = $new_text;
                    }
                }
            }
        });
//dd('OK');
        $text = $this->tagHeadingEncode($text);

        return $text;

    }

    /**
     * Create Replace
     *
     * @param $url
     * @param bool $blank
     * @param bool $nofollow
     * @return string
     */
    public function createReplace($url, $blank = false, $nofollow = false) {
        $attr_target = $blank ? ' target="_blank "' : '';
        $attr_nofollow = $nofollow ? ' rel="nofollow "' : '';
        return "<a title=\"$1\" " . $attr_target . $attr_nofollow . " href=\"$url\">$1</a>";
    }


    /**
     * Decode tag heading h1->h6
     *
     * @param $text
     * @return string|string[]|null
     */
    private function tagHeadingDecode($text) {
        $text = preg_replace_callback('%(<h.*?>)(.*?)(</h.*?>)%si',
            function ($m) {
                return $m[1] . inSpecChar($m[2]) . $m[3];
            },
            $text
        );

        return $text;
    }


    /**
     * Encode tag heading h1->h6
     *
     * @param $text
     * @return string|string[]|null
     */
    private function tagHeadingEncode($text) {
        $text = preg_replace_callback('%(<h.*?>)(.*?)(</h.*?>)%si', function ($m) {
            return $m[1] . reSpecChar($m[2]) . $m[3];
        },
            $text
        );

        return $text;
    }

    /**
     * Setting max single
     *
     * @param int $max_single
     */
    public function setMaxSingle(int $max_single){
        $this->max_single = $max_single;
    }

    /**
     * Setting max keyword
     *
     * @param int $max_keyword
     */
    public function setMaxKeyword($max_keyword){
        $this->max_keyword = $max_keyword;
    }

    /**
     * Setting model keyword
     *
     * @param string $keyword
     */
    public function setModelKeyword($keyword){
        $this->keyword = $keyword;
    }


}
