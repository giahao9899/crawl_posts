<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $table = 'links';

    protected $fillable = ['', 'post_id', 'status'];

    public function post() {
        return $this->belongsTo('App\Models\Post', 'post_id');
    }
}
