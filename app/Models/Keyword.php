<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    protected $fillable = [
        'id', 'name', 'url', 'type', 'nofollow'
    ];

    protected $casts = [
        'nofollow' => 'boolean',
    ];
}
