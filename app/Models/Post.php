<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use kanazaca\CounterCache\CounterCache;

class Post extends Model
{
    use CrudTrait;

    use SoftDeletes;

    use Sluggable;

    use CounterCache;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'posts';
    protected $fillable = ['id', 'descriptions', 'title', 'slug', 'content', 'answer', 'created_by', 'category_id'];
    protected $dates = ['deleted_at'];

    public $counterCacheOptions = [
        'Category' => ['field' => 'post_count', 'foreignKey' => 'category_id']
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function category() {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function link() {
        return $this->hasOne('App\Models\Link', 'post_id');
    }

    public function created_by() {
        return $this->belongsTo('App\Models\BackpackUser', 'created_by');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
