@extends('frontend.layout.app')


@section('content')
<div class="container">
    <h2>{{$post->title}}</h2>

    <div class="content">
        {!! $post_content !!}
    </div>
    <hr>
    <div class="answer">
        {!! $post->answer !!}
    </div>
</div>
    @endsection
