@extends('frontend.layout.app')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-3">
                <div class="card">
                    <div class="card-body">
                        <h4>Danh mục</h4>
                        <ul>
                            @foreach($categories as $category)
                                <li><a href="{{route('home.category', ['id' => $category->id])}}">{{$category->name}}</a> - {{$category->post_count}}</li>
                                @if(!empty($category->children))
                                    <ul>
                                        @foreach($category->children as $child)
                                            <li><a href="{{route('home.category', ['id' => $child->id])}}">{{$child->name}}</a> - {{$child->post_count}}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            @endforeach
                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-9">
                <div class="card">
                    <div class="card-body">
                        <h4>Bài viết</h4>
                        <ul>
                            @foreach($posts as $post)
                                <li class="py-2"><a href="{{route('home.post', ['slug' => $post->slug])}}">{{$post->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="card-footer">
                        {{ $posts->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

